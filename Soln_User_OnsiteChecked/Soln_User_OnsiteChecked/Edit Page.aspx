﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit Page.aspx.cs" Inherits="Soln_User_OnsiteChecked.Edit_Page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task</title>
       <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
     
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript">
        function pageLoad()
        {
            $(document).ready(function ()
            {
                $('#<%= txtfirstname.ClientID%>').attr('disabled', true);
                $('#<%= txtlastname.ClientID%>').attr('disabled', true);
                $('#<%= chkonsite.ClientID%>').attr('disabled', true);
                $('#<%= btnedit.ClientID%>').attr('disabled', true);

                $('#<%= ddluserid.ClientID %>').change(function ()
                {
                     if (this.value == 'others')
                  {
                         $('#<%= txtfirstname.ClientID %>').css('display', 'block');
                         $('#<%= txtlastname.ClientID %>').css('display', 'block');
                         $('#<%= chkonsite.ClientID %>').css('display', 'block');
                         $('#<%= btnedit.ClientID %>').css('display', 'block');
                   }
                     else
                     {
                         $('#<%= txtfirstname.ClientID %>').css('display', 'none');
                         $('#<%= txtlastname.ClientID %>').css('display', 'none');
                         $('#<%= chkonsite.ClientID %>').css('display', 'none');
                         $('#<%= btnedit.ClientID %>').css('display', 'none');
                    }
                });
            });
        }
     </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="w3-table-all" style="width:400px; margin-left: auto;  margin-right: auto; height:200px">
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddluserid" runat="server" DataTextField="Userid" DataValueField="Userid" OnSelectedIndexChanged="ddluserid_SelectedIndexChanged" />
                        </td>
                    </tr>                    
                        <caption>
                            <br />
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtfirstname" runat="server"  CssClass="w3-input w3-animate-input" placeHolder="Firstname" />
                                </td>
                            </tr>
                            <caption>
                                <br />
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtlastname" runat="server"  CssClass="w3-input w3-animate-input" placeHolder="Lastname" />
                                    </td>
                                </tr>
                                <caption>
                                    <br />
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkonsite" runat="server" AutoPostBack="true" />
                                        </td>
                                    </tr>
                                    <caption>
                                        <br />
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblerror" runat="server" />
                                            </td>
                                        </tr>
                                        <caption>
                                            <br />
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnedit" CssClass="w3-button" runat="server" OnClick="btnedit_Click" Text="Edit" />
                                                </td>
                                            </tr>
                                        </caption>
                                    </caption>
                                </caption>
                            </caption>
                    </caption>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div>
    </form>
</body>
</html>
