﻿using Soln_User_OnsiteChecked.Entity;
using Soln_User_OnsiteChecked.Entity.Interface;
using Soln_User_OnsiteChecked.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Soln_User_OnsiteChecked.Dal
{
    public class UserDal
    {
         #region Declaration
        private useronsiteinsertDataContext Dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            Dc = new useronsiteinsertDataContext();
        }

       
        #endregion

        public async  Task<dynamic> login(Iuserentity userentityobj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() => {

                var setQuery =
                    Dc
                    .uspsetuser(
                        "login"
                        ,userentityobj.userid
                        ,userentityobj.firstname
                       , userentityobj.lastname
                        , userentityobj.onsite                    
                        ,ref status
                        ,ref message
                    );

                return message;

            });
        }


         public async Task<userentity> GetUserData(userentity userentityobj)
        {
            return await Task.Run(() => {

                return

                  Dc
                  .uspgetuser(
                      "UserData",
                      userentityobj.userid
                      )
                  .AsEnumerable()
                  .Select(
                        (leUspGetUsersResultSetObj) => new userentity()
                        {
                            Userid = leUspGetUsersResultSetObj.userid,
                            Firstname = leUspGetUsersResultSetObj.firstname,
                            Lastname = leUspGetUsersResultSetObj.lastname,
                            onsite = leUspGetUsersResultSetObj.onsite
                        });

            });
        }
         public async Task<dynamic> updatedata(Iuserentity userentityobj)
         {
             int? status = null;
             String message = null;

             return await Task.Run(() =>
             {

                 var setQuery =
                     Dc
                     .uspsetuser(
                         "update"
                         , userentityobj.userid
                         , userentityobj.firstname
                        , userentityobj.lastname
                         , userentityobj.onsite
                         , ref status
                         , ref message
                     );

                 return message;

             });
         }
    }
}