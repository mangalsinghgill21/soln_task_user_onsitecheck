﻿using Soln_User_OnsiteChecked.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_User_OnsiteChecked.Dal.Interface
{
   public interface IUserRepository
    {
       async Task<bool> login(Iuserentity userentityobj);
    }
}
