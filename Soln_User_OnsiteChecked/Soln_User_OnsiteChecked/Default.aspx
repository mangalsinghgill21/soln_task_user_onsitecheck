﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Async="true" Inherits="Soln_User_OnsiteChecked.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
     
   <%--  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript">
        function pageLoad()
        {
            $(document).ready(function ()
            {


            });
        }
     </script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    
    </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="w3-table-all" style="width:400px; margin-left: auto;  margin-right: auto; height:200px">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtfirstname" CssClass="w3-input w3-animate-input" runat="server" placeholder="FirstName" />
                        </td>
                    </tr>                       
                            <br />
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtlastname" runat="server" CssClass="w3-input w3-animate-input" placeholder="LastName" />
                                </td>
                            </tr>
                           
                                <br />
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkonsite" runat="server" Text="OnSite" AutoPostBack="true" OnCheckedChanged="chkonsite_CheckedChanged" />
                                    </td>
                                </tr>
                                
                                    <br />
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="w3-button" OnClick="btnsubmit_Click" Text="Submit" />
                                        </td>
                                    </tr>
                    <br />
                    <tr>
                        <td>
                            <asp:Label ID="lblerror" runat="server"  />
                        </td>
                    </tr>
                                
                    </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
