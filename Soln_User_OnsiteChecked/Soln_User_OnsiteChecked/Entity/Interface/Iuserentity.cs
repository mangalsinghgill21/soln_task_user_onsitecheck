﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_User_OnsiteChecked.Entity.Interface
{
  public  interface Iuserentity 
    {
         dynamic userid { get; set; }
         dynamic firstname { get; set; }
         dynamic lastname { get; set; }
         dynamic onsite { get; set; }
    }
}
