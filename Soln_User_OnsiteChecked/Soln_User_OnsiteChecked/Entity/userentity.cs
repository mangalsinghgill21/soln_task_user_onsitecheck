﻿using Soln_User_OnsiteChecked.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_User_OnsiteChecked.Entity
{
    public class userentity : Iuserentity
    {
        public dynamic userid { get; set; }
        public dynamic firstname { get; set; }
        public dynamic lastname { get; set; }
        public dynamic onsite { get; set; }
    }
}