﻿using Soln_User_OnsiteChecked.Dal;
using Soln_User_OnsiteChecked.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_User_OnsiteChecked
{
    public partial class Edit_Page : System.Web.UI.Page
    {
        #region Declaration
        private UserDal userdalobj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
             
        }

        protected async void ddluserid_SelectedIndexChanged(object sender, EventArgs e)
        {
             await BindUserData();
        }

        #region private methods
        private async Task BindUserData()
        {
            
            userdalobj = new UserDal();

        
            var getUserData = await userdalobj.GetUserData(new userentity()
            {
                userid = 1
            });

            
            await this.UserMappingEntityToUiData(getUserData);
        }

       
        private async Task UserMappingEntityToUiData(userentity userentityobj)
        {
            await Task.Run(() =>
            {

                // Mapping
                txtfirstname.Text = userentityobj.firstname;
                txtlastname.Text = userentityobj.lastname;
                chkonsite.Checked = userentityobj.onsite;

            });
        }

        private async Task<userentity> UserMappingUiToEntityData()
        {
            return await Task.Run(() =>
            {
                var userentityobj = new userentity()
                {
                    userid = 1,
                    firstname = txtfirstname.Text,
                    lastname = txtlastname.Text,
                    onsite = chkonsite.Checked
                };

                return userentityobj;
            });
        }
        #endregion

        protected async void btnedit_Click(object sender, EventArgs e)
        {
              var userentityobj= await this.UserMappingUiToEntityData();


              userdalobj = new UserDal();
              await userdalobj.updatedata(userentityobj);

        }
    }
}