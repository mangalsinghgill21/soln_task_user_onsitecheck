﻿using Soln_User_OnsiteChecked.Dal;
using Soln_User_OnsiteChecked.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_User_OnsiteChecked
{
    public partial class Default : System.Web.UI.Page
    {
        #region Declaration
        private UserDal userdalobj = null;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtfirstname.Text!=null && txtlastname.Text!= null && chkonsite.Checked==true)
            {
                
            // Mapping User Entity Obj.
            var userentityobj = await this.UserMappingData();

            // Add data
            await AddUserData(userentityobj);
            }
            else
            {
                lblerror.Text = "enter properly";
            }

        }

        #region private method

        private async Task<userentity> UserMappingData()
        {
            return await Task.Run(() =>
            {
                var userentityobj = new userentity()
                {
                    firstname = txtfirstname.Text,
                    lastname = txtlastname.Text,
                    onsite = chkonsite.Checked

                };

                return userentityobj;
            });
        }

         private async Task AddUserData(userentity userentityobj)
        {
            // Create an instance of User Dal.
            userdalobj = new UserDal();

            // add Data to Database and Bind to the lable
            this.lblerror= await userdalobj.login(userentityobj);
        }
        #endregion   

        protected void chkonsite_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}