﻿CREATE PROCEDURE [dbo].[uspsetuser]
	@Command nVarchar(50)=NULL,
	
	@userid Numeric(18,0)=NULL,
	@firstname nVarchar(50)=NULL,
	@lastname nVarchar(50)=NULL,
	@onsite bit = null,
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tbluser
						(
							Userid,
							firstname,
							lastname,
							Onsite
							
						)
						VALUES
						(
							@userid,
							@firstname,
							@lastname,
							@onsite
							
						)
						
			        SET @Status=1
			        SET @Message='Insert Succesfully'
						

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END			
	END
