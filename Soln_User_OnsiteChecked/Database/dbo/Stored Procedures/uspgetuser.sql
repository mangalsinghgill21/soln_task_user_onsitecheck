﻿create PROCEDURE [dbo].[uspgetuser]
	@Command nVarchar(50)=NULL,
	
	@userid Numeric(18,0)=NULL,
	@firstname nVarchar(50)=NULL,
	@lastname nVarchar(50)=NULL,
	@onsite bit = null,
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT
AS
Begin
        
		DECLARE @ErrorMessage varchar(MAX)
		IF @Command='getdata'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						if exists(select u.Firstname ,
								u.Lastname,
								u.Onsite
						 from tbluser as u
						 where u.Userid= @userid) 
	                     Begin
							  set @status=1
		                      set @message='valid user'

							  update tbluser 
							  set Firstname=@firstname ,
							        Lastname=@lastname,
									Onsite=@onsite
						 	  where Userid= @userid				     							   	                SELECT @userid =@@IDENTITY       
						    End
	          	else
						Begin
						set @status=0
						 set @message='invalid user'
						End
						
						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						RAISERROR(@ErrorMessage,16,1)
					END CATCH
					End

End
	
Go